#if !defined(AFX_CLULIST_H__54B5A88F_E969_425C_BE04_CFA9075008B3__INCLUDED_)
#define AFX_CLULIST_H__54B5A88F_E969_425C_BE04_CFA9075008B3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CluList.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCluList dialog
class CScene;
class CEditorDoc;
class CEditorView;

class CCluList : public CDialog
{
// Construction
public:
	CEditorView* m_pView;
	CEditorDoc* m_pDoc;
	CScene* m_pData;
	int m_nIndex;	//给鼠标双击和右键菜单用
	CCluList(CEditorView* pView,CEditorDoc* pDoc,CScene* pData);   // 构造器
	CCluList(CWnd* pParent = NULL);   // standard constructor
	BOOL Create();
	void UpdateList();	//刷新列表显示
// Dialog Data
	//{{AFX_DATA(CCluList)
	enum { IDD = IDD_CLULIST };
	CListCtrl	m_lstCluList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCluList)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCluList)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnEditone();
	afx_msg void OnInsert();
	afx_msg void OnDelone();
	afx_msg void OnAddone();
	afx_msg void OnDblclk(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRclick(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLULIST_H__54B5A88F_E969_425C_BE04_CFA9075008B3__INCLUDED_)
