// Bmp.cpp

#include "stdafx.h"
#include "Game.h"

CBmp::CBmp()
{
	m_bShow = FALSE;
	m_x = 0;
	m_y = 0;
	m_w = 0;
	m_h = 0;
	memset( m_psFileName, 0x0, 256 );
}

CBmp::~CBmp()
{
}

// 初始化，打开资源文件，构造必要的DC
BOOL CBmp::Init( CDC* pDC )
{
	m_w = 0;
	m_h = 0;
	m_MemDC.CreateCompatibleDC( pDC );
	CBitmap* pBmp = new CBitmap;
	pBmp->CreateCompatibleBitmap( pDC, m_w, m_h );
	DeleteObject( m_MemDC.SelectObject( pBmp ) );
	return TRUE;
}

// 打开文件，设置显示位置
BOOL CBmp::Open( char* psFileName, int x, int y, int sx, int sy, int sw, int sh )
{
	HBITMAP hbmp = (HBITMAP)LoadImage( NULL, psFileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE );
	if( hbmp == NULL )
		return FALSE;

	// 保存当前图片的文件名
	memset( m_psFileName, 0x0, 256 );
	strncpy( m_psFileName, psFileName, 255 );

	DeleteObject( m_MemDC.SelectObject(hbmp) );

	m_x = x;
	m_y = y;

	// 如果设定了显示的高宽，则取用设定值
	if( (sw != 0) && (sw != 0) )
	{
		m_sx = sx;
		m_sy = sy;
		m_w = sw;
		m_h = sh;
	}
	else
	{
		// 整图显示，从图片得到宽高属性
		m_sx = 0;
		m_sy = 0;
		// 取得图片的宽高
		CBitmap* pBmp = m_MemDC.GetCurrentBitmap();
		BITMAP stBitmap; 
		pBmp->GetObject( sizeof(BITMAP), &stBitmap );
		m_w = stBitmap.bmWidth;
		m_h = stBitmap.bmHeight;
	}

	// 允许显示
	m_bShow = TRUE;
	return TRUE;
}

// 绘制
void CBmp::Draw( CDC* pDC )
{
	if( !m_bShow )
		return;

	HDC hdc = pDC->GetSafeHdc();
	HDC hMemDC = m_MemDC.GetSafeHdc();
	BitBlt( hdc,m_x,m_y,m_w,m_h,hMemDC,m_sx,m_sy,SRCCOPY );
}

// 设置是否显示
BOOL CBmp::Show( BOOL bShow )
{
	BOOL bTemp = m_bShow;
	m_bShow = bShow;
	return bTemp;
}

// 将本类保存至字符串
BOOL CBmp::SaveToString( CString& strFile )
{
	strFile.Format( "%d;%d;%d;%d;%d;%d;%d;%s;", m_x, m_y, m_w, m_h, m_sx, m_sy, m_bShow, m_psFileName );
	return TRUE;
}

// 从字符串还原本类
BOOL CBmp::LoadFromString( CString& strFile )
{
	CString strTmp;
	int k = 0;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_x = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_y = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_w = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_h = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_sx = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_sy = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	m_bShow = atoi( strTmp );

	k ++;
	if( !GetSlice( strTmp, strFile, k ) )
		return FALSE;
	memset( m_psFileName, 0x0, 256 );
	strncpy( m_psFileName, strTmp, 255 );

	if( strlen(m_psFileName) > 0 )
	{
		// 重新调入图片
		HBITMAP hbmp = (HBITMAP)LoadImage( NULL, m_psFileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE );
		if( hbmp == NULL )
			return FALSE;
		DeleteObject( m_MemDC.SelectObject(hbmp) );
	}

	return TRUE;
}

/* END */