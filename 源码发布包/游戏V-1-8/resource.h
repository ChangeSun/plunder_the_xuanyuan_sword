//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 game.rc 使用
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDC_CURSORA                     106
#define IDR_MAINFRAME                   128
#define IDR_GAMETYPE                    129
#define IDB_BMP_START                   130
#define IDB_BMP_BUF                     131
#define IDB_BMP_MAP                     132
#define IDC_CURSOR2                     134
#define IDC_CURSOR6                     135
#define IDC_CURSOR5                     136
#define IDC_CURSOR4                     137
#define IDC_CURSOR3                     138
#define IDC_CURSOR1                     139
#define IDB_BMP_INFO                    140
#define IDB_BMP_DIALOG                  141
#define IDB_BMP_MINIMAP                 142
#define IDB_BMP_TITLE                   144
#define IDB_BMP_END                     145
#define IDB_BMP_COLORTEXT               146
#define IDB_BMP_BOMB                    147
#define ID_UP                           32771
#define ID_DOWN                         32772
#define ID_LEFT                         32773
#define ID_RIGHT                        32774
#define ID_KILL                         32775
#define ON_REMIND                       32776
#define ON_SNAPSHOT                     32777
#define ON_MAP                          32778
#define ON_EXIT                         32779

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        148
#define _APS_NEXT_COMMAND_VALUE         32780
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
