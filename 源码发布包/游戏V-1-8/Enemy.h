// Enemy.h: interface for the CEnemy class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ENEMY_H__BB11DEE2_6DBF_11D6_BD4D_0080C8F73607__INCLUDED_)
#define AFX_ENEMY_H__BB11DEE2_6DBF_11D6_BD4D_0080C8F73607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CEnemy  
{
public:
	CEnemy();

	static void initStaticData(CDC *pdc);

	void Doinit(int enemytype, CPoint pla);
	void Dowound(int (* npc)[200], int pow, CPoint point, CPoint actpoint, int *pscore);
	void Dodisplay(CDC *Buffer,int (* map)[200], int (* npc)[200],
					int actmovest, CPoint actpoint, int *pblood, int *level);
private:
	int type;//定义类型
	int act_num;//定义运动代号。1上2下3左4右5上左6上右7下左8下右
	//9上走10下走11左走12右走13上攻14下攻15左攻16右攻17左上攻18右上攻19左下攻20右下攻
	//21上伤22下伤23左伤24右伤25左上伤26右上伤27左下伤28右下伤
	//（从不静止，即使位置不动也要切换图片）
	int movest;//定义当前状态：0无动作1,2,3,4上走5,6,7,8下走
	//9,10,11,12左走13,14,15,16右走
	//17,18向上打19,20向下打21,22向左打23,24向右打25,26左上打27,28右上打29,30左下打31,32右下打
	//33,34上伤35,36下伤37,38左伤39,40右伤41,42左上伤43,44右上伤45,46左下伤47,48右下伤
	//动作完成时被置为0，表示可以进行下一个动作。
	int blood;//定义生命
	int power;//定义威力
	int move[20];//定义移动序列
	int cur_act;//当前动作
	CPoint place;//定义位置
	CPoint newplace;//定义新位置（由domove函数计算出）
	CPoint oldplace;//存留老的位置
	RECT rect;//定义目标区域框架

	CDC* pBmpDC;//定义存留设备的内存
	int delay;//定义受到进攻后的呆滞状态的时间

private:
	static HBITMAP bmp1;//定义图片1
	static HBITMAP bmp2;//定义图片2
	static HBITMAP bmp3;//定义图片3
	static HBITMAP bmp4;//定义图片4
	static HBITMAP bmp5;//定义图片5
	static HBITMAP bmp6;//定义图片6
	static CDC bmpDC1;
	static CDC bmpDC2;
	static CDC bmpDC3;
	static CDC bmpDC4;
	static CDC bmpDC5;
	static CDC bmpDC6;
	static BYTE behave[140];//定义怪兽行为的序列

private:
	void goleft(int (* map)[200], int (* npc)[200], CPoint actpoint);//向左走
	void goright(int (* map)[200], int (* npc)[200], CPoint actpoint);//向右走
	void wait(void);//等待
	void closeup(int (* map)[200], int (* npc)[200], CPoint actpoint);//靠近主角
	void runaway(int (* map)[200], int (* npc)[200], CPoint actpoint);//远离主角
	void fire(int *bld, CPoint actpoint, int (* npc)[200], int *level);//发动进攻
	void getrect(int actmovest, CPoint actpoint);//计算当前显示区域
	BOOL isactive(CPoint actpoint);//是否在屏幕范围之内
	void domove(int (* map)[200], int (* npc)[200], CPoint actpoint);//新位置是否可以过去
};

#endif // !defined(AFX_ENEMY_H__BB11DEE2_6DBF_11D6_BD4D_0080C8F73607__INCLUDED_)
