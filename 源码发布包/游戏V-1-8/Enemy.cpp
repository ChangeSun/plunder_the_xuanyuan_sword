// Enemy.cpp: implementation of the CEnemy class.//////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "game.h"
#include "Enemy.h"
#include "mmsystem.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// CEnemy类的静态成员
HBITMAP CEnemy::bmp1;//定义图片1
HBITMAP CEnemy::bmp2;//定义图片2
HBITMAP CEnemy::bmp3;//定义图片3
HBITMAP CEnemy::bmp4;//定义图片4
HBITMAP CEnemy::bmp5;//定义图片5
HBITMAP CEnemy::bmp6;//定义图片6
CDC CEnemy::bmpDC1;
CDC CEnemy::bmpDC2;
CDC CEnemy::bmpDC3;
CDC CEnemy::bmpDC4;
CDC CEnemy::bmpDC5;
CDC CEnemy::bmpDC6;
BYTE CEnemy::behave[140];//定义怪兽行为的序列
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CEnemy::CEnemy()//默认初始化器
{
	type=0;
	movest=0;
	cur_act=0;
	act_num=2;//脸朝下
}
//以下是这个类的成员变量。供参考。
//	int type;//定义类型
//	int act_num;//定义运动方式。1上2下3左4右5上左6上右7下左8下右
	//9上走10下走11左走12右走13上攻14下攻15左攻16右攻17左上攻18右上攻19左下攻20右下攻
	//21上伤22下伤23左伤24右伤25左上伤26右上伤27左下伤28右下伤
	//（从不静止，即使位置不动也要切换图片）
//	int movest;//定义当前状态：0无动作1,2,3,4上走5,6,7,8下走
	//9,10,11,12左走13,14,15,16右走
	//17,18向上打19,20向下打21,22向左打23,24向右打25,26左上打27,28右上打29,30左下打31,32右下打
	//33,34上伤35,36下伤37,38左伤39,40右伤41,42左上伤43,44右上伤45,46左下伤47,48右下伤
//	int blood;//定义生命
//	int power;//定义威力
//	int move[20];//定义移动序列
//	CPoint place;//定义位置
//	RECT rect;//定义目标区域框架
//	HBITMAP bmp;//定义图片
//	CDC bmpDC;//定义存留设备的内存
//	int delay;//定义受到进攻后的呆滞状态的时间
//███████████████████████████████████████
void CEnemy::initStaticData(CDC *pdc)//run only once
{
	FILE *fp=fopen("system\\enemies\\mov.ini","rb");
	fread(behave,sizeof(BYTE),120,fp);//读入动作序列
	fclose(fp);

	bmp1=(HBITMAP)LoadImage(NULL,"system\\enemies\\enemy01.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	bmp2=(HBITMAP)LoadImage(NULL,"system\\enemies\\enemy02.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	bmp3=(HBITMAP)LoadImage(NULL,"system\\enemies\\enemy03.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	bmp4=(HBITMAP)LoadImage(NULL,"system\\enemies\\enemy04.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	bmp5=(HBITMAP)LoadImage(NULL,"system\\enemies\\enemy05.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	bmp6=(HBITMAP)LoadImage(NULL,"system\\enemies\\enemy06.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);

	bmpDC1.CreateCompatibleDC(pdc);
	bmpDC2.CreateCompatibleDC(pdc);
	bmpDC3.CreateCompatibleDC(pdc);
	bmpDC4.CreateCompatibleDC(pdc);
	bmpDC5.CreateCompatibleDC(pdc);
	bmpDC6.CreateCompatibleDC(pdc);

	bmpDC1.SelectObject(bmp1);
	bmpDC2.SelectObject(bmp2);
	bmpDC3.SelectObject(bmp3);
	bmpDC4.SelectObject(bmp4);
	bmpDC5.SelectObject(bmp5);
	bmpDC6.SelectObject(bmp6);
}
//███████████████████████████████████████
void CEnemy::Doinit(int enemytype,CPoint pla)
{
	type=enemytype;
	if(type!=0){//实际的初始化段落
		int i;
		delay=0;
		act_num=1;
		movest=0;
		CPoint point;
		point.x=12;
		point.y=7;
		getrect(0,point);//计算当前显示区域
		place.x=pla.x;
		place.y=pla.y;
		newplace.x=pla.x;
		newplace.y=pla.y;
		oldplace.x=pla.x;
		oldplace.y=pla.y;
		if(type==1){//多足肉球
			blood=200;
			power=5;
			pBmpDC = &bmpDC1;
			for(i=0;i<20;i++){
				move[i]=behave[i];
			}
		}
		else if(type==2){//毒蛇
			blood=300;
			power=10;
			pBmpDC = &bmpDC2;
			for(i=0;i<20;i++){
				move[i]=behave[(20+i)];
			}
		}
		else if(type==3){//骷髅兵
			blood=400;
			power=15;
			pBmpDC = &bmpDC3;
			for(i=0;i<20;i++){
				move[i]=behave[(40+i)];
			}
		}
		else if(type==4){//炎怪
			blood=500;
			power=20;
			pBmpDC = &bmpDC4;
			for(i=0;i<20;i++){
				move[i]=behave[(60+i)];
			}
		}
		else if(type==5){//蜥蜴
			blood=600;
			power=25;
			pBmpDC = &bmpDC5;
			for(i=0;i<20;i++){
				move[i]=behave[(80+i)];
			}
		}
		else if(type==6){//狮子
			blood=3000;
			power=35;
			pBmpDC = &bmpDC6;
			for(i=0;i<20;i++){
				move[i]=behave[(100+i)];
			}
		}
	}
}
//███████████████████████████████████████
void CEnemy::fire(int *bld,CPoint actpoint,int (* npc)[200],int *level)
{
if(type!=0){
	int i=(((place.x-actpoint.x)>0)?(place.x-actpoint.x):(actpoint.x-place.x));
	int j=(((place.y-actpoint.y)>0)?(place.y-actpoint.y):(actpoint.y-place.y));
	if((i<2)&&(j<2)){//敌人与主角临近。下面的程序确定敌人的方向
		sndPlaySound("system\\enemies\\fire.wav",SND_ASYNC|SND_NODEFAULT);
		if((place.x-actpoint.x)>0){//right
			if((place.y-actpoint.y)>0){//down
				act_num=17;
			}
			else if((place.y-actpoint.y)==0){//middle
				act_num=15;
			}
			else if((place.y-actpoint.y)<0){//up
				act_num=19;
			}
		}
		else if((place.x-actpoint.x)==0){//middle
			if((place.y-actpoint.y)>0){//down
				act_num=13;
			}
			else if((place.y-actpoint.y)<0){//up
				act_num=14;
			}
		}
		else if((place.x-actpoint.x)<0){//left
			if((place.y-actpoint.y)>0){//down
				act_num=18;
			}
			else if((place.y-actpoint.y)==0){//middle
				act_num=16;
			}
			else if((place.y-actpoint.y)<0){//up
				act_num=20;
			}
		}
	//定义运动方式。1上2下3左4右5上左6上右7下左8下右
	//9上走10下走11左走12右走13上攻14下攻15左攻16右攻17左上攻18右上攻19左下攻20右下攻
	//21上伤22下伤23左伤24右伤25左上伤26右上伤27左下伤28右下伤
		int s=(*level/10);
		s=(power-s);
		if(s<1){s=1;}//s必须大于1
		if(type==1){//给分数值加数
     		*bld-=s;
		}else if(type==2){
			*bld-=s;
		}else if(type==3){
			*bld-=s;
		}else if(type==4){
			*bld-=s;
		}else if(type==5){
			*bld-=s;
		}else if(type==6){
			*bld-=s;
		}
		if(*bld<0){*bld=0;}
		else if(*bld>200){*bld=200;}
	}
	else{//如果打不到，则等待。
		wait();
	}
}
}
//███████████████████████████████████████
void CEnemy::Dowound(int (* npc)[200],int pow,CPoint point,CPoint actpoint,int *pscore)
{
if((type!=0)&&(isactive(actpoint))){//如果是空类型则不处理
	if((point.x>=rect.left)&&(point.x<=rect.right)
		&&(point.y>=rect.top)&&(point.y<=rect.bottom)){//检验是否被击中。
		sndPlaySound("system\\enemies\\wound.wav",SND_ASYNC|SND_NODEFAULT);
		blood-=pow;//减血
		delay=4;//暂停一格
		if(npc[oldplace.x][oldplace.y]==type){
			npc[oldplace.x][oldplace.y]=0;
		}
		oldplace.x=place.x;
		oldplace.y=place.y;
		place.x=newplace.x;
		place.y=newplace.y;//如果在运动中，则提前进入运动完成状态
		if(blood<0){//被打死
			blood=0;
			if((*pscore>200)||(*pscore<0)){*pscore=0;}//消除不正常的数值
			if(type==2){//给分数值加数
				*pscore+=2;
			}else if(type==3){
				*pscore+=3;
			}else if(type==4){
				*pscore+=4;
			}else if(type==5){
				*pscore+=5;
			}else if(type==6){
				*pscore+=6;
			}else{
        		*pscore+=1;
			}
			npc[place.x][place.y]=0;
			type=0;//该人物停止活动
		}
	}
}
}
//███████████████████████████████████████
void CEnemy::Dodisplay(CDC *Buffer,int (* map)[200],int (* npc)[200],
						int actmovest,CPoint actpoint,int *pblood,int *plevel)
{
if(type!=0){//如果是空类型则不处理。如果不显示什么，那么它在地图上不会出现
if(isactive(actpoint)){//不在屏幕范围之内的不处理
//如果动作完成，则从动作序列中读出下一个动作。
	if((delay==0)&&(movest==0)){
		switch(move[cur_act]){
		case '1':goleft(map,npc,actpoint);break;
		case '2':goright(map,npc,actpoint);break;
		case '3':wait();break;
		case '4':closeup(map,npc,actpoint);break;
		case '5':runaway(map,npc,actpoint);break;
		case '6':fire(pblood,actpoint,npc,plevel);break;
		}
		cur_act++;
		if(cur_act==19){
			cur_act=0;
		}
		//根据act_num确定相应的movest
		switch(act_num){
		case 1:movest=49;break;
		case 2:movest=49;break;
		case 3:movest=49;break;
		case 4:movest=49;break;
		case 5:movest=49;break;
		case 6:movest=49;break;
		case 7:movest=49;break;
		case 8:movest=49;break;
		case 9:movest=1;break;
		case 10:movest=5;break;
		case 11:movest=9;break;
		case 12:movest=13;break;
		case 13:movest=17;break;
		case 14:movest=19;break;
		case 15:movest=21;break;
		case 16:movest=23;break;
		case 17:movest=25;break;
		case 18:movest=27;break;
		case 19:movest=29;break;
		case 20:movest=31;break;
		case 21:movest=33;break;
		case 22:movest=35;break;
		case 23:movest=37;break;
		case 24:movest=39;break;
		case 25:movest=41;break;
		case 26:movest=43;break;
		case 27:movest=45;break;
		case 28:movest=47;break;
		}
	}
	if(delay!=0){//delay期间一定是受伤状态
		switch(act_num){
		case 1:act_num=21;break;
		case 2:act_num=22;break;
		case 3:act_num=23;break;
		case 4:act_num=24;break;
		case 5:act_num=25;break;
		case 6:act_num=26;break;
		case 7:act_num=27;break;
		case 8:act_num=28;break;
		case 9:act_num=21;break;
		case 10:act_num=22;break;
		case 11:act_num=23;break;
		case 12:act_num=24;break;
		case 13:act_num=21;break;
		case 14:act_num=22;break;
		case 15:act_num=23;break;
		case 16:act_num=24;break;
		case 17:act_num=25;break;
		case 18:act_num=26;break;
		case 19:act_num=27;break;
		case 20:act_num=28;break;}
		switch(act_num){
		case 1:movest=49;break;
		case 2:movest=49;break;
		case 3:movest=49;break;
		case 4:movest=49;break;
		case 5:movest=49;break;
		case 6:movest=49;break;
		case 7:movest=49;break;
		case 8:movest=49;break;
		case 9:movest=1;break;
		case 10:movest=5;break;
		case 11:movest=9;break;
		case 12:movest=13;break;
		case 13:movest=17;break;
		case 14:movest=19;break;
		case 15:movest=21;break;
		case 16:movest=23;break;
		case 17:movest=25;break;
		case 18:movest=27;break;
		case 19:movest=29;break;
		case 20:movest=31;break;
		case 21:movest=33;break;
		case 22:movest=35;break;
		case 23:movest=37;break;
		case 24:movest=39;break;
		case 25:movest=41;break;
		case 26:movest=43;break;
		case 27:movest=45;break;
		case 28:movest=47;break;}
	}
//	int act_num;//定义运动方式。1上2下3左4右5上左6上右7下左8下右
	//9上走10下走11左走12右走13上攻14下攻15左攻16右攻17左上攻18右上攻19左下攻20右下攻
	//21上伤22下伤23左伤24右伤25左上伤26右上伤27左下伤28右下伤
	//（从不静止，即使位置不动也要切换图片）
//	int movest;//定义当前状态：0动作完成1,2,3,4上走5,6,7,8下走9,10,11,12左走13,14,15,16右走
	//17,18向上打19,20向下打21,22向左打23,24向右打25,26左上打27,28右上打29,30左下打31,32右下打
	//33,34上伤35,36下伤37,38左伤39,40右伤41,42左上伤43,44右上伤45,46左下伤47,48右下伤
//以下是显示程序段
	int c=0;
	int d=0;
	switch(movest){//根据movest显示。
	case 0:
		switch(act_num){
		case 1:c=32;d=64;break;
		case 2:c=32;d=0;break;
		case 3:c=64;d=32;break;
		case 4:c=0;d=32;break;
		case 5:c=64;d=64;break;
		case 6:c=0;d=64;break;
		case 7:c=64;d=0;break;
		case 8:c=0;d=0;break;
		}break;
	case 1:c=128;d=64;break;
	case 2:c=32;d=64;break;
	case 3:c=128;d=64;break;
	case 4:c=32;d=64;break;
	case 5:c=128;d=0;break;
	case 6:c=32;d=0;break;
	case 7:c=128;d=0;break;
	case 8:c=32;d=0;break;
	case 9:c=160;d=32;break;
	case 10:c=64;d=32;break;
	case 11:c=160;d=32;break;
	case 12:c=64;d=32;break;
	case 13:c=96;d=32;break;
	case 14:c=0;d=32;break;
	case 15:c=96;d=32;break;
	case 16:c=0;d=32;break;
	case 17:c=224;d=64;break;
	case 18:c=32;d=64;break;
	case 19:c=224;d=0;break;
	case 20:c=32;d=0;break;
	case 21:c=256;d=32;break;
	case 22:c=64;d=32;break;
	case 23:c=192;d=32;break;
	case 24:c=0;d=32;break;
	case 25:c=256;d=64;break;
	case 26:c=64;d=64;break;
	case 27:c=192;d=64;break;
	case 28:c=0;d=64;break;
	case 29:c=256;d=0;break;
	case 30:c=64;d=0;break;
	case 31:c=192;d=0;break;
	case 32:c=0;d=0;break;
	case 33:c=320;d=64;break;
	case 34:c=320;d=64;break;
	case 35:c=320;d=0;break;
	case 36:c=320;d=0;break;
	case 37:c=352;d=32;break;
	case 38:c=352;d=32;break;
	case 39:c=288;d=32;break;
	case 40:c=288;d=32;break;
	case 41:c=352;d=64;break;
	case 42:c=352;d=64;break;
	case 43:c=288;d=64;break;
	case 44:c=288;d=64;break;
	case 45:c=352;d=0;break;
	case 46:c=352;d=0;break;
	case 47:c=288;d=0;break;
	case 48:c=288;d=0;break;
	case 49:
		switch(act_num){
		case 1:c=32;d=64;break;
		case 2:c=32;d=0;break;
		case 3:c=64;d=32;break;
		case 4:c=0;d=32;break;
		case 5:c=64;d=64;break;
		case 6:c=0;d=64;break;
		case 7:c=64;d=0;break;
		case 8:c=0;d=0;break;
		}break;
	case 50:
		switch(act_num){
		case 1:c=128;d=64;break;
		case 2:c=128;d=0;break;
		case 3:c=160;d=32;break;
		case 4:c=96;d=32;break;
		case 5:c=160;d=64;break;
		case 6:c=96;d=64;break;
		case 7:c=160;d=0;break;
		case 8:c=96;d=0;break;
		}break;
	}
	//定义当前状态：0无动作1,2,3,4上走5,6,7,8下走9,10,11,12左走13,14,15,16右走
	//17,18向上打19,20向下打21,22向左打23,24向右打25,26左上打27,28右上打29,30左下打31,32右下打
	//33,34上伤35,36下伤37,38左伤39,40右伤41,42左上伤43,44右上伤45,46左下伤47,48右下伤49,50等待
	getrect(actmovest,actpoint);//计算当前显示区域
	Buffer->BitBlt(rect.left,rect.top,32,32,pBmpDC,c,(d+96),SRCAND);//拷贝蒙版
	Buffer->BitBlt(rect.left,rect.top,32,32,pBmpDC,c,d,SRCINVERT);//拷贝图像

	//动作完成后，要修改movest
	if(delay==0){
		movest++;//进入下一个动作
		if((movest==5)||(movest==9)||(movest==13)||(movest==17)){
			oldplace.x=place.x;
			oldplace.y=place.y;
			place.x=newplace.x;
			place.y=newplace.y;//place的改变要在走动完成最后一步时才实现。
		}
		if((movest==5)||(movest==9)||(movest==13)||(movest==17)||
			(movest==19)||(movest==21)||(movest==23)||(movest==25)||
			(movest==27)||(movest==29)||(movest==31)||(movest==33)||
			(movest==35)||(movest==37)||(movest==39)||(movest==41)||
			(movest==43)||(movest==45)||(movest==47)||(movest==49)||(movest==51)){
			movest=0;
		}
	}
	else{
		delay--;//如果延时还没有完成，则继续刚才的僵化状态
	}
}
}
}
//███████████████████████████████████████
void CEnemy::goleft(int (* map)[200],int (* npc)[200],CPoint actpoint)//向左走
{
	switch(act_num){
	case 1:act_num=11;break;
	case 2:act_num=12;break;
	case 3:act_num=10;break;
	case 4:act_num=9;break;
	case 5:act_num=11;break;
	case 6:act_num=9;break;
	case 7:act_num=10;break;
	case 8:act_num=11;break;
	case 9:act_num=11;break;
	case 10:act_num=12;break;
	case 11:act_num=10;break;
	case 12:act_num=9;break;
	case 13:act_num=11;break;
	case 14:act_num=12;break;
	case 15:act_num=10;break;
	case 16:act_num=9;break;
	case 17:act_num=12;break;
	case 18:act_num=9;break;
	case 19:act_num=10;break;
	case 20:act_num=11;break;
	//1上2下3左4右5上左6上右7下左8下右
	//9上走10下走11左走12右走13上攻14下攻15左攻16右攻17左上攻18右上攻19左下攻20右下攻
	//21上伤22下伤23左伤24右伤25左上伤26右上伤27左下伤28右下伤
	}
	domove(map,npc,actpoint);
}
void CEnemy::goright(int (* map)[200],int (* npc)[200],CPoint actpoint)//向右走
{
	switch(act_num){
	case 1:act_num=12;break;
	case 2:act_num=11;break;
	case 3:act_num=9;break;
	case 4:act_num=10;break;
	case 5:act_num=9;break;
	case 6:act_num=12;break;
	case 7:act_num=11;break;
	case 8:act_num=10;break;
	case 9:act_num=12;break;
	case 10:act_num=11;break;
	case 11:act_num=9;break;
	case 12:act_num=10;break;
	case 13:act_num=12;break;
	case 14:act_num=11;break;
	case 15:act_num=9;break;
	case 16:act_num=10;break;
	case 17:act_num=9;break;
	case 18:act_num=12;break;
	case 19:act_num=11;break;
	case 20:act_num=10;break;
	//1上2下3左4右5上左6上右7下左8下右
	//9上走10下走11左走12右走13上攻14下攻15左攻16右攻17左上攻18右上攻19左下攻20右下攻
	//21上伤22下伤23左伤24右伤25左上伤26右上伤27左下伤28右下伤
	}
	domove(map,npc,actpoint);
}
void CEnemy::wait(void)//等待
{
	switch(act_num){
	case 9:act_num=1;break;
	case 10:act_num=2;break;
	case 11:act_num=3;break;
	case 12:act_num=4;break;
	case 13:act_num=1;break;
	case 14:act_num=2;break;
	case 15:act_num=3;break;
	case 16:act_num=4;break;
	case 17:act_num=5;break;
	case 18:act_num=6;break;
	case 19:act_num=7;break;
	case 20:act_num=8;break;
	case 21:act_num=1;break;
	case 22:act_num=2;break;
	case 23:act_num=3;break;
	case 24:act_num=4;break;
	case 25:act_num=5;break;
	case 26:act_num=6;break;
	case 27:act_num=7;break;
	case 28:act_num=8;break;
	//1上2下3左4右5上左6上右7下左8下右
	//9上走10下走11左走12右走13上攻14下攻15左攻16右攻17左上攻18右上攻19左下攻20右下攻
	//21上伤22下伤23左伤24右伤25左上伤26右上伤27左下伤28右下伤
	}
}
void CEnemy::closeup(int (* map)[200],int (* npc)[200],CPoint actpoint)//靠近主角
{
	int a=(place.x-actpoint.x);
	int b=(place.y-actpoint.y);
	int i=((a>0)?a:(actpoint.x-place.x));
	int j=((b>0)?b:(actpoint.y-place.y));
	if(i>j){//应当x方向移动
		if(a>0){
			act_num=11;
		}
		else{
			act_num=12;
		}
	}
	else{//应当y方向移动
		if(b>0){
			act_num=9;
		}
		else{
			act_num=10;
		}
	}
	domove(map,npc,actpoint);
}
void CEnemy::runaway(int (* map)[200],int (* npc)[200],CPoint actpoint)//远离主角
{
	int a=(place.x-actpoint.x);
	int b=(place.y-actpoint.y);
	int i=((a>0)?a:(actpoint.x-place.x));
	int j=((b>0)?b:(actpoint.y-place.y));
	if(i>j){//应当x方向移动
		if(a>0){
			act_num=12;
		}
		else{
			act_num=11;
		}
	}
	else{//应当y方向移动
		if(b>0){
			act_num=10;
		}
		else{
			act_num=9;
		}
	}
	domove(map,npc,actpoint);
}
//███████████████████████████████████████
void CEnemy::getrect(int actmovest,CPoint actpoint)//计算当前显示区域
{
	int c=(place.x-actpoint.x);
	int d=(place.y-actpoint.y);
	rect.left=(383+32*c);
	rect.top=(223+32*d);
	//当主角未发生位移时，应在的位置
	c=0;
	d=0;
	switch(actmovest){
	case 1:c=0;d=8;break;
	case 2:c=0;d=16;break;
	case 3:c=0;d=24;break;
	case 4:c=0;d=32;break;
	case 5:c=0;d=-8;break;
	case 6:c=0;d=-16;break;
	case 7:c=0;d=-24;break;
	case 8:c=0;d=-32;break;
	case 9:c=8;d=0;break;
	case 10:c=16;d=0;break;
	case 11:c=24;d=0;break;
	case 12:c=32;d=0;break;
	case 13:c=-8;d=0;break;
	case 14:c=-16;d=0;break;
	case 15:c=-24;d=0;break;
	case 16:c=-32;d=0;break;
	}
	rect.left+=c;
	rect.top+=d;
	//^当主角偏离原位时修正后的位置
	c=0;
	d=0;
	switch(movest){
	case 1:c=0;d=-8;break;
	case 2:c=0;d=-16;break;
	case 3:c=0;d=-24;break;
	case 4:c=0;d=-32;break;
	case 5:c=0;d=8;break;
	case 6:c=0;d=16;break;
	case 7:c=0;d=24;break;
	case 8:c=0;d=32;break;
	case 9:c=-8;d=0;break;
	case 10:c=-16;d=0;break;
	case 11:c=-24;d=0;break;
	case 12:c=-32;d=0;break;
	case 13:c=8;d=0;break;
	case 14:c=16;d=0;break;
	case 15:c=24;d=0;break;
	case 16:c=32;d=0;break;
	}
	rect.left+=c;
	rect.top+=d;
	rect.right=(rect.left+32);
	rect.bottom=(rect.top+32);
	//^根据自己的移动情况修正后的位置
	//定义当前状态：0无动作1,2,3,4上走5,6,7,8下走9,10,11,12左走13,14,15,16右走
	//17,18向上打19,20向下打21,22向左打23,24向右打25,26左上打27,28右上打29,30左下打31,32右下打
	//33,34上伤35,36下伤37,38左伤39,40右伤41,42左上伤43,44右上伤45,46左下伤47,48右下伤
}
BOOL CEnemy::isactive(CPoint actpoint)//是否在屏幕范围之内
{
	int i=(((place.x-actpoint.x)>0)?(place.x-actpoint.x):(actpoint.x-place.x));
	int j=(((place.y-actpoint.y)>0)?(place.y-actpoint.y):(actpoint.y-place.y));
	if((i<15)&&(j<10)){
		return(TRUE);
	}
	else{
		return(FALSE);
	}
}
void CEnemy::domove(int (* map)[200],int (* npc)[200],CPoint actpoint)//新位置是否可以过去
{
	switch(act_num){
	case 9:newplace.x=place.x;newplace.y=(place.y-1);break;
	case 10:newplace.x=place.x;newplace.y=(place.y+1);break;
	case 11:newplace.x=(place.x-1);newplace.y=place.y;break;
	case 12:newplace.x=(place.x+1);newplace.y=place.y;break;
	default:wait();return;
	}
	int i=map[newplace.x][newplace.y];
	int j=npc[newplace.x][newplace.y];
	if((i<201)&&(i>0)&&(j<20))
	{//可以运动
		npc[oldplace.x][oldplace.y]=0;
		npc[place.x][place.y]=type;
		npc[newplace.x][newplace.y]=(type+10);
		oldplace=place;
	}
	else
	{//不可以运动
		newplace.x=place.x;
		newplace.y=place.y;//维持位置不变
		wait();
	}
}
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
