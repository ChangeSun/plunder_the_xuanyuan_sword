// macro.h

#ifndef __MACRO_H__
#define __MACOR_H__

// 地图“瓷砖”的宽度和高度
#define TILE_W             (32)
#define TILE_H             (32)

// 地图编辑区能显示多少块“瓷砖”
#define EDIT_AREA_GRID_W   (21)
#define EDIT_AREA_GRID_H   (21)
#define EDIT_AREA_W        (TILE_W * EDIT_AREA_GRID_W)
#define EDIT_AREA_H        (TILE_H * EDIT_AREA_GRID_H)
#define EDIT_PAGE_STEP     (10)

// “瓷砖”的逻辑宽度和逻辑高度
#define MAP_TILES_LOGIC_W  (40)
#define MAP_TILES_LOGIC_H  (20)
#define MAP_TILES_NUM_MAX  (MAP_TILES_LOGIC_W * MAP_TILES_LOGIC_W)
#define MAP_TILES_ZERO     (0)//0表示空

// “瓷砖”上部5行是可以行走的，其余部分不可通行
#define TILE_PASSABLE_ROW_COUNT  (5)

// 地图的大小，单位是“瓷砖”，包括编辑区以外看不见的部分
#define MAP_GRID_W         (200)
#define MAP_GRID_H         (200)

// 滚动条的宽度
#define SCROLL_BAR_W       (20)
// 右编辑区的位置
#define RIGHT_EDIT_AREA_X_OFFSET    (EDIT_AREA_W + SCROLL_BAR_W)

// 信息栏的Y位置。信息栏在编辑区下方
#define INFO_BAR_Y         (EDIT_AREA_H + SCROLL_BAR_W + 4)


#endif